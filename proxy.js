/**
 * Created by WeiChen on 2016/5/5.
 */
'use strict'
var Http = require('http'),
    Router = require('router'),
    server,
    router = new Router();

server = Http.createServer(function (request, response) {
    router(request, response, function (error) {
        if (!error) {
            response.writeHead(404);
        } else {
            // Handle errors
            console.log(error.message, error.stack);
            response.writeHead(400);
        }
        response.end('API Server is running!');
    });
});

server.listen(3000, function () {
    console.log('Listening on port 3000');
});

// Get 'eris-contracts'.
var erisContracts = require('eris-contracts');
// Get 'eris-db' (the javascript API for eris-db)
var erisdbModule = require("eris-db");
// Create a new instance of ErisDB that uses the given URL.
var erisdb = erisdbModule.createInstance("http://140.119.19.234:1337/rpc");
// The private key.
var accountData = require('./accounts.json');
var pipe = new erisContracts.pipes.DevPipe(erisdb, accountData.iotchain_full_000);
pipe.addAccount(accountData.iotchain_validator_001);
pipe.addAccount(accountData.iotchain_validator_003);
var chain = erisdb.blockchain();
var account = erisdb.accounts();
var transaction = erisdb.txs();
var contractManager = erisContracts.newContractManager(pipe);
var contractData = require('./epm.json');
var address = contractData["deployContract"];
var fs = require('fs');
var abi = JSON.parse(fs.readFileSync("./abi/" + address));
var iotManager;
var created = false;
//set price
var price = 100;
contractManager.newContractFactory(abi).at(address, function (err, contract) {
    if (err) {
        throw err;
    }
    console.log("New Contract: " + contract.address);
    contract.setPrepay(price, function (err, res) {
        if (err) {
            console.log(err);
            throw err;
        }
        console.log("set prepay: " + res);
    });
    created = true;
    iotManager = contract;
})
function getOrCreateContract(request, response) {
    response.writeHead(201, {
        'Content-Type': 'text/plain'
    });
    response.end(iotManager.address);
}

var BodyParser = require('body-parser');
router.use(BodyParser.text());
function getOrCreatePurchase(request, response) {
    var addr = request.params.addr;
    response.writeHead(201, {
        'Content-Type': 'text/plain'
    });
    console.log("Client addr: " + addr);
    iotManager.getOrCreatePurchase(addr, function (err, res) {
        if (err) {
            console.log(err);
            throw err;
            response.end(0);
        }
        console.log(res);
        response.end(res);
    });

}

function getBalance(request, response){
    var addr = request.params.addr;
    response.writeHead(201, {
        'Content-Type': 'text/plain'
    });
    console.log("Client addr: " + addr);
    iotManager.getBalance(addr, function (err, res) {
        if (err) {
            console.log(err);
            throw err;
            response.end(0);
        }
        console.log(res);
        response.end(res);
    });
}
router.get('/get', getOrCreateContract);
router.get('/create/:addr', getOrCreatePurchase);
router.get('/getBalance/:addr', getBalance);