/**
 * Created by WeiChen on 2016/5/3.
 */
var Client = require('node-rest-client').Client;
var client = new Client();

var fs = require('fs');
var accountData = require('./accounts.json');
// Create a new pipe.
var erisContracts = require('eris-contracts');
var erisdbModule = require("eris-db");
var erisdb = erisdbModule.createInstance("http://140.119.19.235:1337/rpc");
var pipe = new erisContracts.pipes.DevPipe(erisdb, accountData.iotchain_validator_001);
pipe.addAccount(accountData.iotchain_validator_003);

var contractData = require('./epm.json');
var address = contractData["deployContract"];
var abi = JSON.parse(fs.readFileSync("./abi/" + address));
var contractManager = erisContracts.newContractManager(pipe);
// Create a factory for the contract with the JSON interface 'myAbi'.
var contractFactory = contractManager.newContractFactory(abi);

var address;
//contract object
var iotManager;

// call service and get contract address
function getAddress() {
    client.get("http://140.119.19.230:3000/get", function (data, response) {
        address = data.toString();
    });
}

function createContract(){
    contractFactory.at(address, function(error, contract){
        if(error) {throw error}
        console.log("Created: " + contract.address);
        iotManager = contract;
    });
    //tell manager to create purchase contract
    client.get("http://140.119.19.230:3000/create/91C7AE2F1F98608C636D3BF578F4ACA79C7FD9C0", function(data, response){
        console.log(data.toString());
    })
}

function prepay(){
    //prepay
    // {from: accountData.iotchain_validator_001.address, amount: 101, fee:1}
    // iotManager.prepay.sendTransaction({amount: 101, fee:1},function(err, res){
    //     if(err) {
    //         console.log(err);
    //         throw err;
    //     }
    //     console.log("Prepay: " + res);
    // });
    var sequence;
    var address = accountData.iotchain_validator_001.address.toString();
    var pubKey = accountData.iotchain_validator_001.pubKey.toString();
    client.get("http://140.119.19.230:46657/get_account?address=\""+address+"\"", function(data, response){
        sequence = data.result[1].account.sequence + 1;
        console.log(sequence);
    });
    client.post("http://140.119.19.230:46657")
    var args={
        "input":     {
            "address":   address,
            "amount":    "101",
            "sequence":  sequence.toString(),
            "signature": "",
            "pub_key":   pubKey
        },
        "address":   "B8FDD06A1B6A49BF6B089DDD576E9653B17BADE7",
        "gas_limit": "5",
        "fee":      "1",
        "data":     "prepay"
    }
    //client.post("http://140.119.19.230:46657/")
}

// function main(){
//     client.get("http://140.119.19.230:3000/prepaid")
// }
getAddress();
createContract();
prepay();